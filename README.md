This repository is WIP and will conatin various string search algorithms implemented in C and x86 assembly, so that we can perform a suite of tests and determine which implementation is faster, and which algorithm is better suited for a certain problem, as well.

For the moment, available options are:

* Navigate to a folder containing an algorithm (for e.g., cd KMP)
* 'make all' will compile/assemble & link everything
* 'make runc' compiles and runs the C variant of the algorithm
* 'make runasm' assembles and runs the x86 Intel-flavoured nasm assembly version of the algorithm
* 'make clean' cleans off binaries generated by the above commands

Code is compiled into 32-bit ELF executables. Can be slightly tweaked to work on Microsoft Windows, as well: the C version is portable and should run out of the box. Assembly version should have a few function names changed (e.g. printf to _printf as required by the OS). Recommend running the code under Linux Subsystem (Bash) in Windows 10.

Implementing a specification required by a class project for Analysis of Algorithms, 2nd Year, 2017-2018, Faculty of Automatic Control and Computer Science, University Politehnica of Bucharest.

Coded by Valentin-Gabriel Radu, licensed under GPLv3.
